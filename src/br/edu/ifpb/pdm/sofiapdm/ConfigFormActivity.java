package br.edu.ifpb.pdm.sofiapdm;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import br.edu.ifpb.pdm.sofiapdm.dao.DAOConfigApp;
import br.edu.ifpb.pdm.sofiapdm.utils.ConfigApp;

public class ConfigFormActivity extends Activity {

	private DAOConfigApp daoconfig;
	private ConfigApp ca;
	private EditText etNumeroLetras;
	private EditText etEmail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ca = new ConfigApp();
		setContentView(R.layout.activity_config_form);
		this.daoconfig = new DAOConfigApp(this);
		List<ConfigApp> calist = this.daoconfig.getAll();
		if (calist.size() > 0) {
			this.ca = calist.get(0);
		}
		this.etNumeroLetras = (EditText) findViewById(R.id.etNumeroLetras);
		this.etEmail = (EditText) findViewById(R.id.etEmail);
		this.etNumeroLetras.setText(String.valueOf(ca.getLetras()));
		this.etEmail.setText(ca.getEmail());
	}

	public void submeter(View view) {

		this.etNumeroLetras = (EditText) findViewById(R.id.etNumeroLetras);
		this.etEmail = (EditText) findViewById(R.id.etEmail);

		int numeroLetras = Integer
				.parseInt(etNumeroLetras.getText().toString());
		String email = etEmail.getText().toString();

		ca.setLetras(numeroLetras);

		ca.setEmail(email);
		this.daoconfig.save(ca);
		Intent it = new Intent();
		setResult(RESULT_OK, it);
		finish();

	}
}
