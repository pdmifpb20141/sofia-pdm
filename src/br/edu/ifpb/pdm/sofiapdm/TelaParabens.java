package br.edu.ifpb.pdm.sofiapdm;

import java.util.List;

import br.edu.ifpb.pdm.sofiapdm.dao.DAOConfigApp;
import br.edu.ifpb.pdm.sofiapdm.utils.ClienteGmail;
import br.edu.ifpb.pdm.sofiapdm.utils.ConfigApp;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class TelaParabens extends Activity {

	protected DAOConfigApp daoca;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_parabens);
		ConfigApp ca = new ConfigApp();

		this.daoca = new DAOConfigApp(this);
		List<ConfigApp> calist = this.daoca.getAll();
		if (calist.size() > 0) {
			ca = calist.get(0);
		}
		if (ca.getEmail().length() > 0) {

			try {
				ClienteGmail sender = new ClienteGmail("jogoletras@gmail.com",
						"jogoletras12");

				sender.sendMail(
						"Jogo de Letras",
						String.format(
								"Olá responsável\nSeu filho concluiu a atividade de %d letras.\n.Mude o nível de dificuldade no aplicativo apertando no botão Configurações.",
								ca.getLetras()), "jogoletras@gmail.com", ca
								.getEmail());
			} catch (Exception e) {
				Log.i("Enviar Email", e.getMessage(), e);
			}
		}
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent i = new Intent(TelaParabens.this, HomeActivity.class);
				startActivity(i);
				MediaPlayer mp = MediaPlayer.create(getApplicationContext(),
						R.raw.chord);
				mp.start();
				// close this activity
				finish();
			}
		}, 3000);
	}
}
