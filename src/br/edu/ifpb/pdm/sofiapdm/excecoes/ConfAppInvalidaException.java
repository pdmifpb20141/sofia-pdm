package br.edu.ifpb.pdm.sofiapdm.excecoes;

public class ConfAppInvalidaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConfAppInvalidaException(String msgerror){
		super(msgerror);
	}
}
