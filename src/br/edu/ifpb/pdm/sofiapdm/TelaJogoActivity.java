package br.edu.ifpb.pdm.sofiapdm;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import br.edu.ifpb.pdm.sofiapdm.dao.DAOConfigApp;
import br.edu.ifpb.pdm.sofiapdm.models.ImagemPalavra;
import br.edu.ifpb.pdm.sofiapdm.utils.ConfigApp;

public class TelaJogoActivity extends Activity {

	protected DAOConfigApp daoconfig;
	protected ConfigApp ca;
	protected List<ImagemPalavra> ips;
	protected int state;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.ca = new ConfigApp();
		this.daoconfig = new DAOConfigApp(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_jogo);
		this.state = 0;
		this.ips = new ArrayList<ImagemPalavra>();
		List<ConfigApp> calist = this.daoconfig.getAll();
		if(calist.size() > 0){
			ca = calist.get(0);
		}
		switch (ca.getLetras()) {
		case 5:
			ips.add(new ImagemPalavra("porco", R.drawable.porco));
			ips.add(new ImagemPalavra("bruxa", R.drawable.bruxa));
			ips.add(new ImagemPalavra("porta", R.drawable.porta));
			break;
		case 6:
			ips.add(new ImagemPalavra("dragão", R.drawable.dragao));
			ips.add(new ImagemPalavra("planta", R.drawable.planta));
			ips.add(new ImagemPalavra("coelho", R.drawable.coelho));
			break;	
		default:
			ips.add(new ImagemPalavra("vaca", R.drawable.vacalouca));
			ips.add(new ImagemPalavra("cama", R.drawable.cama));
			ips.add(new ImagemPalavra("bola", R.drawable.bola));
			break;
		}
		atualizaTela();
	}

	public void atualizaTela() {
		ImagemPalavra ip = ips.get(state);
		ImageView iv = (ImageView) findViewById(R.id.imagem);
		EditText et = (EditText) findViewById(R.id.editText1);
		iv.setImageResource(ip.getSrc());
		et.setText("");

	}

	public void verificarPalavra(View view) {

		EditText et = (EditText) findViewById(R.id.editText1);
		String resposta = et.getText().toString();
		if (resposta.equals(ips.get(state).getNome())) {

			MediaPlayer mp = MediaPlayer.create(getApplicationContext(),
					R.raw.beep9);
			mp.start();
			state++;
			if (state >= 3) {
				Intent it = new Intent(this, TelaParabens.class);
				startActivity(it);
				finish();
				return;
			}
		} else {
			MediaPlayer mp = MediaPlayer.create(getApplicationContext(),
					R.raw.beep7);
			mp.start();
			if (state > 0) {
				state--;
			}
		}
		atualizaTela();
	}
}
