package br.edu.ifpb.pdm.sofiapdm.utils;

import java.io.Serializable;

public class ConfigApp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int Id;
	private int letras;
	private String email;
	
	public ConfigApp(){
		Id = -1;
		letras = 4;
		email = "";
	}

	public int getLetras() {
		return letras;
	}

	public void setLetras(int letras) {
		this.letras = letras;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}
}
