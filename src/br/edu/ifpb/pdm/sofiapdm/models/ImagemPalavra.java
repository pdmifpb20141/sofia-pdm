package br.edu.ifpb.pdm.sofiapdm.models;

public class ImagemPalavra {

	private String nome;
	private int src;
	
	public ImagemPalavra(String nome, int src) {
		super();
		this.nome = nome.toUpperCase();
		this.src = src;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getSrc() {
		return src;
	}

	public void setSrc(int src) {
		this.src = src;
	}
	
	
}
