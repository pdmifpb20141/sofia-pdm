package br.edu.ifpb.pdm.sofiapdm;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import br.edu.ifpb.pdm.sofiapdm.dao.DAOConfigApp;
import br.edu.ifpb.pdm.sofiapdm.utils.ConfigApp;

public class HomeActivity extends Activity {

	
	private static final int CONF_APP = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
	}
	
	public void iniciar(View view){
		Intent it = new Intent(this, TelaJogoActivity.class);
		startActivity(it);
		finish();
	}
	
	public void configurar(View view){
		Intent it = new Intent(this, ConfigFormActivity.class);
		startActivityForResult(it, CONF_APP);
	}
	
	public void sair(View view){
		finish();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			if(requestCode == CONF_APP){
				Toast.makeText(this, "Configuração salva com sucesso!", Toast.LENGTH_SHORT).show();
			}
		}
		else{
			Toast.makeText(this, "Erro na operação", Toast.LENGTH_SHORT).show();
		}
	}
	
}
