package br.edu.ifpb.pdm.sofiapdm.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.edu.ifpb.pdm.sofiapdm.excecoes.ConfAppInvalidaException;
import br.edu.ifpb.pdm.sofiapdm.utils.ConfigApp;

public class DAOConfigApp implements DAO<ConfigApp, Integer> {

	private SQLiteDatabase banco;

	public DAOConfigApp(Context context) {
		this.banco = context.openOrCreateDatabase("sofia.db",
				Context.MODE_PRIVATE, null);
		this.create();
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE IF NOT EXISTS ConfigApp(");
		sb.append("Id integer primary key autoincrement,");
		sb.append(" Dificuldade integer,");
		sb.append(" Email text);");

		this.banco.execSQL(sb.toString());

	}

	@Override
	public boolean save(ConfigApp novo) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put("Dificuldade", novo.getLetras());
		cv.put("Email", novo.getEmail());
		if (novo.getId() == -1) {
			return this.banco.insert("ConfigApp", null, cv) > 0;
		} else {
			String id = String.valueOf(novo.getId());
			return this.banco.update("ConfigApp", cv, "Id = ?",
					new String[] { id }) > 0;
		}
	}

	@Override
	public ConfigApp get(int Id) {
		// TODO Auto-generated method stub
		String colunas[] = new String[] { "Id", "Dificuldade", "Email" };
		String coluna[] = new String[] { Integer.toString(Id) };

		Cursor c = this.banco.query("ConfigApp", colunas, "Id=?", coluna, null,
				null, null);

		ConfigApp ca;

		if (c.moveToNext()) {
			ca = new ConfigApp();

			ca.setId(c.getInt(c.getColumnIndex("Id")));
			ca.setLetras(c.getInt(c.getColumnIndex("Dificuldade")));
			ca.setEmail(c.getString(c.getColumnIndex("Email")));

			return ca;
		}
		throw new ConfAppInvalidaException(String.format("Id %d inválido", Id));
	}

	@Override
	public List<ConfigApp> getAll() {
		// TODO Auto-generated method stub
		String colunas[] = new String[] { "Id", "Dificuldade", "Email" };

		Cursor c = this.banco.query("ConfigApp", colunas, null, null, null,
				null, null);

		List<ConfigApp> caList = new ArrayList<ConfigApp>();

		ConfigApp ca;

		while (c.moveToNext()) {
			ca = new ConfigApp();

			ca.setId(c.getInt(c.getColumnIndex("Id")));
			ca.setLetras(c.getInt(c.getColumnIndex("Dificuldade")));
			ca.setEmail(c.getString(c.getColumnIndex("Email")));

			caList.add(ca);
		}
		return caList;
	}

}
