package br.edu.ifpb.pdm.sofiapdm.dao;

import java.util.List;

public interface DAO<T, K> {

	public void create();
	public boolean save(T novo);
	public T get(int codigo);
	public List<T> getAll();
}
